# Ova aplikacija napravljena ju u sklopu projekta: "Moderno obrazovanje stručnih prvostupnika/ca mehatronike usklađeno sa zahtjevima HKO-a" 

Cilj projekta je unaprijeđen program preddiplomskog stručnog studija Mehatronika uz razvijen standard zanimanja inženjera/ke mehatronike i standard kvalifikacije usklađen sa smjernicama za izradu i vrednovanje standarda kvalifikacija te smjernicama za usklađivane studijskih programa sa standardima kvalifikacija.

### Projekt je financiran u iznosu od od 2.825.655,13 kn iz Europskog socijalnog fonda, OP Učinkoviti ljudski potencijali 2014-2020. Proovedba projekta traje 24 mjeseca (od ožujka 2019. do ožujka 2021. godine).

- Službena stranica projekta: https://vub.hr/hko-mehatronika/

- Aplikacija je napravljena za automatiziranu obradu ankete, koja je predložena prema projektu. 

![](slika.png)
