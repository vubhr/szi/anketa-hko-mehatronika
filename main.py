# -*- coding: utf-8 -*-
import os
import sys
from PIL import ImageTk, Image
from tkinter import Tk, font, StringVar, filedialog, scrolledtext, END, IntVar
from tkinter.ttk import Button, Label, Entry, Radiobutton
import pandas as pd
import math
from reportlab.platypus import SimpleDocTemplate, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, TA_LEFT, TA_CENTER
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import letter
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.fonts import addMapping


UVODNO_PITANJE1 = "Ocijenite svoje općenito zadovoljstvo s jasnoćom informacija dobivenih na kolegiju na ljestvici od 1 (izrazito nezadovoljan) do 5 (izrazito zadovoljan)."
UVODNO_PITANJE2 = "Ciljevi i zahtjevi kolegija jasno su izloženi na početku nastave."
UVODNO_PITANJE3 = "Ishodi učenja jasno su objašnjeni i znate točno što se očekuje od vas."
UVODNO_PITANJE4 = "Informirani ste o obavezama, metodama provjere znanja i načinu ocjenjivanja."
UVODNO_PITANJE5 = "Upoznati ste sa literaturom i materijalima potrebnim za savladavanje kolegija."
UVODNO_PITANJE6 = "Potrebni materijali za učenje i izvršavanje zadaća dostupni su i odgovarajući."
UVODNO_PITANJE7 = "Oprema (hardverska i softverska) dostupna za provedbu ovog kolegija je adekvatna."
UVODNO_PITANJE8 = "Pohvale"
UVODNO_PITANJE9 = "Prijedlozi"
UVODNO_PITANJE10 = "Kritike"

ZAVRSNO_PITANJE1 = "Ocijenite svoje općenito zadovoljstvo s jasnoćom informacija dobivenih na kolegiju na ljestvici od 1 (Izrazito nezadovoljan) do 5 (izrazito zadovoljan)."
ZAVRSNO_PITANJE2 = "Kolegij je bio dobro organiziran."
ZAVRSNO_PITANJE3 = "Zadovoljan sam stečenim teorijskim i praktičnim znanjima iz ovog kolegija."
ZAVRSNO_PITANJE4 = "Sadržaj koji se obrađivao na predmetu koristiti će mi za budući posao."
ZAVRSNO_PITANJE5 = "Obaveze na kolegiju usklađene su s ECTS bodovima (1 bod = 25 do 30 sati rada)."
ZAVRSNO_PITANJE6 = "Provjere znanja se provode transparentno."
ZAVRSNO_PITANJE7 = "Zadaci na svim provjerama znanja (kolokviji, vježbe) pokrivali su ishode učenja kolegija."
ZAVRSNO_PITANJE8 = "Ocjenjivanje na kolokvijima i vježbama je dalo korisnu povratnu informaciju."
ZAVRSNO_PITANJE9 = "Ocijenite na ljestvici od 1 (izrazito nezadovoljan) do 5 (izrazito zadovoljan), koliko ste zadovoljni sveukupnim radom i pristupom nastavnika."
ZAVRSNO_PITANJE10 = "Pristupom obradi teme nastavnik je pobuđivao zanimanje za predavanje."
ZAVRSNO_PITANJE11 = "Teme predavanja bile su izložene na razumljiv način."
ZAVRSNO_PITANJE12 = "Nastavnik je poticao diskusiju, interakciju i zanimanje za temu."
ZAVRSNO_PITANJE13 = "Nastavnik se prema studentima odnosio korektno i s poštovanjem."
ZAVRSNO_PITANJE14 = "Nastavnik je bio dostupan te pružao korisne povratne informacije o mom radu."
ZAVRSNO_PITANJE15 = "Nastavnik dobro poznaje i vlada svojim područjem."
ZAVRSNO_PITANJE16 = "Ocijenite na ljestvici od 1 (izrazito nezadovoljan) do 5 (izrazito zadovoljan), koliko ste zadovoljni sveukupnim radom i pristupom asistenta."
ZAVRSNO_PITANJE17 = "Pristupom obradi teme asistent je pobuđivao zanimanje za vježbe."
ZAVRSNO_PITANJE18 = "Teme vježbi bile su izložene na razumljiv način."
ZAVRSNO_PITANJE19 = "Asistent je poticao diskusiju, interakciju i zanimanje za temu."
ZAVRSNO_PITANJE20 = "Asistent se prema studentima odnosio korektno i s poštovanjem."
ZAVRSNO_PITANJE21 = "Asistent je bio dostupan te je pružao korisne povratne informacije o mom radu."
ZAVRSNO_PITANJE22 = "Asistent dobro poznaje i vlada svojim područjem."
ZAVRSNO_PITANJE23 = "Koliko sati ste prosječno utrošili na kolegiju tjedno na dodatni rad kod kuće (zadaće, priprema za ispite, projekti)?"
ZAVRSNO_PITANJE24 = "Pohvale"
ZAVRSNO_PITANJE25 = "Prijedlozi"
ZAVRSNO_PITANJE26 = "Kritike"


class App:

    def __init__(self, master: Tk) -> None:
        self.master = master
        self.questionnaire_type = IntVar()
        self.csv_path = StringVar()
        self.pdf_report_path = StringVar()
        self.create_widgets()
        msg = "Aplikacija za analizu anketa zahtjeva .csv dokument s rezultatima ankete iz Google obrazaca. " \
              "Pitanja u obrascu moraju odgovarati identično pitanjima koja su postavljena unutar aplikacije i predložena projektom.\n" \
               "\n Upute za korištenje: \n"\
               "\n1. Preuzmite sve rezultate ankete u obliku .csv dokumenata \n"\
                "\n2. Svaki .csv dokument prilikom preuzimanja nazovite po predmetu. Primjerice: Matematika 1.csv, Osnove programiranja.csv...\n"\
               "\n3.  Napravite direktorij/mapu koji će sadržavati sve .csv dokumente \n" \
                "\n4.  Napravite direktorij/mapu u koji će se spremati izvještaju u obliku PDF dokumenata.\n" \
                "\n5. Odaberite o kakvoj se anketi radi, prosljedite putanje do direktorija u kojem su dokumenti i gdje želite " \
              "spremiti izvještaj. Navdene direktorije ste trebali napraviti u predhodnim koracima\n" \
                "\n6. Pritisnite generiraj izvješta i ako je sve uredu dobiti ćete izvještaje.\n" \
                "\nAplikacija je dostupna na GitLabu Veleučilišta u Bjelovaru i može se prilagoditi, https://gitlab.com/vubhr/szi/anketa-hko-mehatronika.\n"
        self.log_to_console(msg, 'normal')

    def create_widgets(self):

        default_font = font.nametofont("TkDefaultFont")
        default_font.configure(family="Times New Roman", size=16)

        label_csv_path = Label(self.master, text="Putanja do svih CSV rezultata ankete")
        label_csv_path.pack(padx=5, pady=5)

        entry_csv_path = Entry(self.master, width=100, font=("Times New Roman", 14), textvariable=self.csv_path)
        entry_csv_path.pack(padx=5, pady=5)
        btn_csv_path = Button(self.master, text="Odaberi putanju", command=self.get_csv_path)
        btn_csv_path.pack(padx=5, pady=5)

        rb_start = Radiobutton(self.master, text="Uvodna anketa", variable=self.questionnaire_type, value=1)
        rb_start.pack(padx=5, pady=5)


        rb_end = Radiobutton(self.master, text="Završna anketa", variable=self.questionnaire_type, value=2)
        rb_end.pack(padx=5, pady=5)

        label_csv_path = Label(self.master, text="Putanja gdje će se spremiti izvještaji ankete")
        label_csv_path.pack(padx=5, pady=5)

        entry_pdf_path = Entry(self.master, width=100, font=("Times New Roman", 14), textvariable=self.pdf_report_path)
        entry_pdf_path.pack(padx=5, pady=5)
        btn_pdf_path = Button(self.master, text="Odaberi putanju", command=self.get_pdf_path)
        btn_pdf_path.pack(padx=5, pady=5)

        self.infobox = scrolledtext.ScrolledText(self.master, wrap="word",  width=100, height=10, font=("Times New Roman", 14))
        self.infobox.pack(padx=5, pady=5)
        self.infobox.tag_config("warning", foreground="red")
        self.infobox.tag_config("normal", foreground="black")

        btn_generate_report = Button(self.master, text="Generiraj izvještaj", command=self.get_values)
        btn_generate_report.pack(padx=5, pady=5)

        image = Image.open("Resources/logo.png")
        if image is None:
            print("No image!")
        render = ImageTk.PhotoImage(image)
        img = Label(self.master, image=render)
        img.image = render
        img.pack()

    def log_to_console(self, msg, flag):
        self.infobox.configure(state='normal')
        self.infobox.insert(END, msg + '\n', flag)
        self.infobox.configure(state='disabled')
        self.infobox.yview(END)


    def get_values(self):
        if self.questionnaire_type.get() == 1:
            msg = "Odabrali ste uvodnu anketu."
            self.log_to_console(msg, 'Normal')
            if self.csv_path.get() == "":
                msg = "Niste unesli putanju do .csv dokumenata, provjerite putanju!"
                self.log_to_console(msg, 'warning')
            elif self.pdf_report_path.get() == "":
                msg = "Niste unesli putanju do mjesta za pohranu izvještaja, provjerite putanju!"
                self.log_to_console(msg, 'warning')
            else:
                msg = "Odabrali ste sljedeće putanje: \n" \
                      + self.csv_path.get() + "\n" \
                      + self.pdf_report_path.get()
                self.log_to_console(msg, 'Normal')
                data_path = self.csv_path.get()
                out_path = self.pdf_report_path.get()
                for filename in os.listdir(data_path):
                    if filename.endswith(".csv"):
                        full = os.path.join(data_path, filename)
                        msg = "Analizira se sljedeći dokument: " + full
                        self.log_to_console(msg, 'Normal')
                        self.generate_report_questionnaire_start(full, out_path)

        elif self.questionnaire_type.get() == 2:
            msg = "Odabrali ste završnu anketu."
            self.log_to_console(msg, 'Normal')
            if self.csv_path.get() == "":
                msg = "Niste unesli putanju do .csv dokumenata, provjerite putanju!"
                self.log_to_console(msg, 'warning')
            elif self.pdf_report_path.get() == "":
                msg = "Niste unesli putanju do mjesta za pohranu izvještaja, provjerite putanju!"
                self.log_to_console(msg, 'warning')
            else:
                msg = "Odabrali ste sljedeće putanje: \n" \
                      + self.csv_path.get() + "\n" \
                      + self.pdf_report_path.get()
                self.log_to_console(msg, 'Normal')
                data_path = self.csv_path.get()
                out_path = self.pdf_report_path.get()
                for filename in os.listdir(data_path):
                    if filename.endswith(".csv"):
                        full = os.path.join(data_path, filename)
                        msg = "Analizira se sljedeći dokument: " + full
                        self.log_to_console(msg, 'Normal')
                        self.generate_report_questionnaire_end(full, out_path)
        else:
            msg = "Provjerite jeste li odabrali tip ankete!"
            self.log_to_console(msg, 'warning')

    def get_csv_path(self):
        folder_selected = filedialog.askdirectory()
        self.csv_path.set(folder_selected)

    def get_pdf_path(self):
        folder_selected = filedialog.askdirectory()
        self.pdf_report_path.set(folder_selected)


    def generate_report_questionnaire_end(self, data_path, output_path):

        data = pd.read_csv(data_path, sep=",")
        base = os.path.basename(data_path)
        file_name = os.path.splitext(base)[0]
        #print("Svi podaci: \n", data)

        pdfmetrics.registerFont(TTFont('Roboto', 'Resources/Fonts/Roboto-Medium.ttf'))
        addMapping("Roboto", 0, 0, "Roboto-Medium")

        styles = getSampleStyleSheet()

        styles.add(ParagraphStyle(name="TMain", alignment=TA_CENTER, fontName="Roboto", fontSize=16, spaceBefore=12, spaceAfter=12))
        styles.add(ParagraphStyle(name="T1", alignment=TA_LEFT, fontName="Roboto", fontSize=12, spaceBefore=6, spaceAfter=6))
        styles.add(ParagraphStyle(name="C1", alignment=TA_LEFT, fontName="Roboto", fontSize=10, spaceBefore=2, spaceAfter=2, leftIndent=6))

        content = []
        name = file_name
        file_name = file_name + ".pdf"
        pdf_name = os.path.join(output_path, file_name)
        msg = "Stvara se sljedeći izvještaj: " + pdf_name
        self.log_to_console(msg, 'Normal')
        doc = SimpleDocTemplate(
            pdf_name,
            pagesize=letter,
            bottomMargin=2 * cm,
            topMargin=1 * cm,
            rightMargin=2 * cm,
            leftMargin=1 * cm)


        title = str("Završna anketa, predmet: " + name)
        PTITLE = Paragraph(title, styles["TMain"])
        content.append(PTITLE)

        try:
            P1 = Paragraph(ZAVRSNO_PITANJE1, styles["T1"])
            content.append(P1)
            P1O = Paragraph(str(round(data[ZAVRSNO_PITANJE1].mean(), 2)), styles["C1"])
            content.append(P1O)
        except:
            msg = "Problem s pitanjem 1. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P2 = Paragraph(ZAVRSNO_PITANJE2, styles["T1"])
            content.append(P2)
            P2O = Paragraph(str(round(data[ZAVRSNO_PITANJE2].mean(), 2)), styles["C1"])
            content.append(P2O)
        except:
            msg = "Problem s pitanjem 2. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P3 = Paragraph(ZAVRSNO_PITANJE3, styles["T1"])
            content.append(P3)
            P3O = Paragraph(str(round(data[ZAVRSNO_PITANJE3].mean(), 2)), styles["C1"])
            content.append(P3O)

        except:
            msg = "Problem s pitanjem 3. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P4 = Paragraph(ZAVRSNO_PITANJE4, styles["T1"])
            content.append(P4)
            P4O = Paragraph(str(round(data[ZAVRSNO_PITANJE4].mean(), 2)), styles["C1"])
            content.append(P4O)
        except:
            msg = "Problem s pitanjem 4. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P5 = Paragraph(ZAVRSNO_PITANJE5, styles["T1"])
            content.append(P5)
            P5O = Paragraph(str(round(data[ZAVRSNO_PITANJE5].mean(), 2)), styles["C1"])
            content.append(P5O)
        except:
            msg = "Problem s pitanjem 5. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P6 = Paragraph(ZAVRSNO_PITANJE6, styles["T1"])
            content.append(P6)
            P6O = Paragraph(str(round(data[ZAVRSNO_PITANJE6].mean(), 2)), styles["C1"])
            content.append(P6O)
        except:
            msg = "Problem s pitanjem 6. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P7 = Paragraph(ZAVRSNO_PITANJE7, styles["T1"])
            content.append(P7)
            P7O = Paragraph(str(round(data[ZAVRSNO_PITANJE7].mean(), 2)), styles["C1"])
            content.append(P7O)
        except:
            msg = "Problem s pitanjem 7. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        try:
            P8 = Paragraph(ZAVRSNO_PITANJE8, styles["T1"])
            content.append(P8)
            P8O = Paragraph(str(round(data[ZAVRSNO_PITANJE8].mean(), 2)), styles["C1"])
            content.append(P8O)
        except:
            msg = "Problem s pitanjem 8. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P9 = Paragraph(ZAVRSNO_PITANJE9, styles["T1"])
            content.append(P9)
            P9O = Paragraph(str(round(data[ZAVRSNO_PITANJE9].mean(), 2)), styles["C1"])
            content.append(P9O)
        except:
            msg = "Problem s pitanjem 9. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P10 = Paragraph(ZAVRSNO_PITANJE10, styles["T1"])
            content.append(P10)
            P10O = Paragraph(str(round(data[ZAVRSNO_PITANJE10].mean(), 2)), styles["C1"])
            content.append(P10O)
        except:
            msg = "Problem s pitanjem 10. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P11 = Paragraph(ZAVRSNO_PITANJE11, styles["T1"])
            content.append(P11)
            P11O = Paragraph(str(round(data[ZAVRSNO_PITANJE11].mean(), 2)), styles["C1"])
            content.append(P11O)
        except:
            msg = "Problem s pitanjem 11. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P12 = Paragraph(ZAVRSNO_PITANJE12, styles["T1"])
            content.append(P12)
            P12O = Paragraph(str(round(data[ZAVRSNO_PITANJE12].mean(), 2)), styles["C1"])
            content.append(P12O)
        except:
            msg = "Problem s pitanjem 12. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P13 = Paragraph(ZAVRSNO_PITANJE13, styles["T1"])
            content.append(P13)
            P13O = Paragraph(str(round(data[ZAVRSNO_PITANJE13].mean(), 2)), styles["C1"])
            content.append(P13O)
        except:
            msg = "Problem s pitanjem 13. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P14 = Paragraph(ZAVRSNO_PITANJE14, styles["T1"])
            content.append(P14)
            P14O = Paragraph(str(round(data[ZAVRSNO_PITANJE14].mean(), 2)), styles["C1"])
            content.append(P14O)
        except:
            msg = "Problem s pitanjem 14. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        try:
            P15 = Paragraph(ZAVRSNO_PITANJE15, styles["T1"])
            content.append(P15)
            P15O = Paragraph(str(round(data[ZAVRSNO_PITANJE15].mean(), 2)), styles["C1"])
            content.append(P15O)
        except:
            msg = "Problem s pitanjem 15. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P16 = Paragraph(ZAVRSNO_PITANJE16, styles["T1"])
            content.append(P16)
            P16O = Paragraph(str(round(data[ZAVRSNO_PITANJE16].mean(), 2)), styles["C1"])
            content.append(P16O)
        except:
            msg = "Problem s pitanjem 16. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P17 = Paragraph(ZAVRSNO_PITANJE17, styles["T1"])
            content.append(P17)
            P17O = Paragraph(str(round(data[ZAVRSNO_PITANJE17].mean(), 2)), styles["C1"])
            content.append(P17O)
        except:
            msg = "Problem s pitanjem 17. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P18 = Paragraph(ZAVRSNO_PITANJE18, styles["T1"])
            content.append(P18)
            P18O = Paragraph(str(round(data[ZAVRSNO_PITANJE18].mean(), 2)), styles["C1"])
            content.append(P18O)
        except:
            msg = "Problem s pitanjem 18. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        try:
            P19 = Paragraph(ZAVRSNO_PITANJE19, styles["T1"])
            content.append(P19)
            P19O = Paragraph(str(round(data[ZAVRSNO_PITANJE19].mean(), 2)), styles["C1"])
            content.append(P19O)
        except:
            msg = "Problem s pitanjem 19. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P20 = Paragraph(ZAVRSNO_PITANJE20, styles["T1"])
            content.append(P20)
            P20O = Paragraph(str(round(data[ZAVRSNO_PITANJE20].mean(), 2)), styles["C1"])
            content.append(P20O)
        except:
            msg = "Problem s pitanjem 20. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P21 = Paragraph(ZAVRSNO_PITANJE21, styles["T1"])
            content.append(P21)
            P21O = Paragraph(str(round(data[ZAVRSNO_PITANJE21].mean(), 2)), styles["C1"])
            content.append(P21O)
        except:
            msg = "Problem s pitanjem 21. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P22 = Paragraph(ZAVRSNO_PITANJE22, styles["T1"])
            content.append(P22)
            P22O = Paragraph(str(round(data[ZAVRSNO_PITANJE22].mean(), 2)), styles["C1"])
            content.append(P22O)
        except:
            msg = "Problem s pitanjem 22. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        try:
            P23 = Paragraph(ZAVRSNO_PITANJE23, styles["T1"])
            content.append(P23)
            comments = data[ZAVRSNO_PITANJE23].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Odgovor: " + str(comment), styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 23. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P24 = Paragraph(ZAVRSNO_PITANJE24, styles["T1"])
            content.append(P24)
            comments = data[ZAVRSNO_PITANJE24].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Komentar: " + str(comment), styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 24. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P25 = Paragraph(ZAVRSNO_PITANJE25, styles["T1"])
            content.append(P25)
            comments = data[ZAVRSNO_PITANJE25].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Komentar: " + str(comment), styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 25. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P26 = Paragraph(ZAVRSNO_PITANJE26, styles["T1"])
            content.append(P26)
            comments = data[ZAVRSNO_PITANJE26].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Komentar: " + str(comment), styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 26. završne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        doc.build(content,)


    def generate_report_questionnaire_start(self, data_path, output_path):

        data = pd.read_csv(data_path, sep=",")
        base = os.path.basename(data_path)
        file_name = os.path.splitext(base)[0]
        #print("Svi podaci: \n", data)

        pdfmetrics.registerFont(TTFont('Roboto', 'Resources/Fonts/Roboto-Medium.ttf'))
        addMapping("Roboto", 0, 0, "Roboto-Medium")

        styles = getSampleStyleSheet()

        styles.add(ParagraphStyle(name="TMain", alignment=TA_CENTER, fontName="Roboto", fontSize=16, spaceBefore=12,
                                  spaceAfter=12))
        styles.add(
            ParagraphStyle(name="T1", alignment=TA_LEFT, fontName="Roboto", fontSize=12, spaceBefore=6, spaceAfter=6))
        styles.add(
            ParagraphStyle(name="C1", alignment=TA_LEFT, fontName="Roboto", fontSize=10, spaceBefore=2, spaceAfter=2, leftIndent=6))

        content = []

        file_name = file_name + ".pdf"
        pdf_name = os.path.join(output_path, file_name)
        msg = "Stvara se sljedeći izvještaj: " + pdf_name
        self.log_to_console(msg, 'Normal')
        doc = SimpleDocTemplate(
            pdf_name,
            pagesize=letter,
            bottomMargin=2 * cm,
            topMargin=1 * cm,
            rightMargin=2 * cm,
            leftMargin=1 * cm)

        title = str("Uvodna anketa, predmet: " + file_name)
        PTITLE = Paragraph(title, styles["TMain"])
        content.append(PTITLE)

        try:
            P1 = Paragraph(UVODNO_PITANJE1, styles["T1"])
            content.append(P1)
            P1O = Paragraph(str(round(data[UVODNO_PITANJE1].mean(), 2)), styles["C1"])
            content.append(P1O)
        except:
            msg = "Problem s pitanjem 1. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P2 = Paragraph(UVODNO_PITANJE2, styles["T1"])
            content.append(P2)
            P2O = Paragraph(str(round(data[UVODNO_PITANJE2].mean(), 2)), styles["C1"])
            content.append(P2O)
        except:
            msg = "Problem s pitanjem 2. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P3 = Paragraph(UVODNO_PITANJE3, styles["T1"])
            content.append(P3)
            P3O = Paragraph(str(round(data[UVODNO_PITANJE3].mean(), 2)), styles["C1"])
            content.append(P3O)
        except:
            msg = "Problem s pitanjem 3. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P4 = Paragraph(UVODNO_PITANJE4, styles["T1"])
            content.append(P4)
            P4O = Paragraph(str(round(data[UVODNO_PITANJE4].mean(), 2)), styles["C1"])
            content.append(P4O)
        except:
            msg = "Problem s pitanjem 4. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P5 = Paragraph(UVODNO_PITANJE5, styles["T1"])
            content.append(P5)
            P5O = Paragraph(str(round(data[UVODNO_PITANJE5].mean(), 2)), styles["C1"])
            content.append(P5O)
        except:
            msg = "Problem s pitanjem 5. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P6 = Paragraph(UVODNO_PITANJE6, styles["T1"])
            content.append(P6)
            P6O = Paragraph(str(round(data[UVODNO_PITANJE6].mean(), 2)), styles["C1"])
            content.append(P6O)
        except:
            msg = "Problem s pitanjem 6. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P7 = Paragraph(UVODNO_PITANJE7, styles["T1"])
            content.append(P7)
            P7O = Paragraph(str(round(data[UVODNO_PITANJE7].mean(), 2)), styles["C1"])
            content.append(P7O)
        except:
            msg = "Problem s pitanjem 7. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')

        try:
            P8 = Paragraph(UVODNO_PITANJE8, styles["T1"])
            content.append(P8)
            comments = data[UVODNO_PITANJE8].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Komentar: " + comment, styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 8. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        try:
            P9 = Paragraph(UVODNO_PITANJE8, styles["T1"])
            content.append(P9)
            comments = data[UVODNO_PITANJE9].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Komentar: " + comment, styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 9. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        try:
            P10 = Paragraph(UVODNO_PITANJE10, styles["T1"])
            content.append(P10)
            comments = data[UVODNO_PITANJE10].to_list()
            for comment in comments:
                if (not isNan(comment)):
                    para = Paragraph("Komentar: " + comment, styles["C1"])
                    content.append(para)
        except:
            msg = "Problem s pitanjem 10. uvodne ankete, provjerite tekst pitanja."
            self.log_to_console(msg, 'warning')


        doc.build(content,)


def isNan(string):
    return string != string
if __name__ == "__main__":
    root = Tk()
    root.resizable(False, False)

    root.geometry("1000x800")
    root.title("Analiza ankete - HKO mehatronika")
    root.iconbitmap("Resources/vub.ico")

    def on_closing():
            root.destroy()
    root.protocol("WM_DELETE_WINDOW", on_closing)
    app = App(root)
    root.mainloop()
